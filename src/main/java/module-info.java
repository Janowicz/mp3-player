module mp3Player {
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.media;
    requires jid3lib;

    exports mp3player.main to javafx.graphics;
    opens mp3player.controller to javafx.fxml;
    opens mp3player.mp3 to javafx.base;
}