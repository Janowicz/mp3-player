package mp3player.mp3;

import org.farng.mp3.MP3File;
import org.farng.mp3.TagException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SongFileParser {

    public static Song createSong(File file) {
        try {
            MP3File mp3File = new MP3File(file);
            String path = file.getAbsolutePath();
            String title = file.getName();
            String author = mp3File.getID3v2Tag().getLeadArtist();
            String album = mp3File.getID3v2Tag().getAlbumTitle();
            return new Song(title, author, album, path);
        } catch (TagException |
                IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Song> createSongsList(File directory) {
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException("Select a directory.");
        }
        List<Song> songList = new ArrayList<>();
        File[] listFiles = directory.listFiles();
        for (File file : listFiles) {
            if (file.getName().endsWith(".mp3")) {
                Song song = createSong(file);
                songList.add(song);
            }
        }
        return songList;
    }
}
