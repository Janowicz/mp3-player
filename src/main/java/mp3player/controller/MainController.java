package mp3player.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import mp3player.mp3.Song;
import mp3player.mp3.SongFileParser;
import mp3player.player.Mp3Player;
import org.farng.mp3.TagException;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class MainController {

    private Mp3Player mp3Player;
    private static final String stopImage = ControlPaneController.class.getResource("/img/stop.png").toExternalForm();
    private static final String playImage = ControlPaneController.class.getResource("/img/play.png").toExternalForm();

    @FXML
    private MenuPaneController menuPaneController;

    @FXML
    private ContentPaneController contentPaneController;

    @FXML
    private ControlPaneController controlPaneController;

    public void initialize() throws IOException, TagException {
        createPlayer();
        configureTable();
        configurePrevNextButtons();
        configurePlayButton();
        openFileMenu();
        openDiretoryMenu();
    }

    public void createPlayer() {
        ObservableList<Song> songs = contentPaneController.getContentTable().getItems();
        mp3Player = new Mp3Player(songs);
    }

    public void configureTable() {
        TableView<Song> contentTable = contentPaneController.getContentTable();
        contentTable.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            int clickCount = mouseEvent.getClickCount();
            if (clickCount == 2) {
                playSelectedSong(contentTable.getSelectionModel().getSelectedIndex());
            }
        });
    }

    public void playSelectedSong(int selectedSongIndex) {
        mp3Player.loadSong(selectedSongIndex);
        configureProgressSlider();
        configureVolume();
        controlPaneController.getPlayButton().setSelected(true);
        printMessage("Loaded: " + contentPaneController.getContentTable().getItems().get(selectedSongIndex).getTitle());
    }

    public void configureProgressSlider() {
        Slider progressSlider = controlPaneController.getProgressSlider();
        mp3Player.getMediaPlayer().setOnReady(() -> progressSlider.setMax(mp3Player.getSongLength()));
        mp3Player.getMediaPlayer().currentTimeProperty().addListener((observableValue, duration, t1) -> {
                progressSlider.setValue(t1.toSeconds());
        });

        progressSlider.setLabelFormatter(convertTimeToMinutesAndSeconds());

        progressSlider.valueProperty().addListener((observableValue, number, t1) -> {
            if (progressSlider.isValueChanging()) {
                mp3Player.getMediaPlayer().seek(Duration.seconds(t1.doubleValue()));
            }
        });
    }

    private StringConverter<Double> convertTimeToMinutesAndSeconds(){
        StringConverter<Double> stringConverter = new StringConverter<>() {
            @Override
            public String toString(Double value) {
                long second = value.longValue();
                long minutes = TimeUnit.SECONDS.toMinutes(second);
                long remainingSeconds = second - TimeUnit.MINUTES.toSeconds(minutes);
                return String.format("%02d", minutes) + ":" + String.format("%02d", remainingSeconds);
            }

            @Override
            public Double fromString(String s) {
                return null;
            }
        };
        return stringConverter;
    }

    public void configureVolume() {
        Slider volumeSlider = controlPaneController.getVolumeSlider();
        volumeSlider.valueProperty().unbind();
        volumeSlider.setMax(1.0);
        volumeSlider.valueProperty().bindBidirectional(mp3Player.getMediaPlayer().volumeProperty());
    }

    public void configurePrevNextButtons() {
        TableView<Song> contentTable = contentPaneController.getContentTable();
        Button nextButton = controlPaneController.getNextButton();
        Button previousButton = controlPaneController.getPreviousButton();

        nextButton.setOnAction(actionEvent -> {
            contentTable.getSelectionModel().select(contentTable.getSelectionModel().getSelectedIndex() + 1);
            playSelectedSong(contentTable.getSelectionModel().getSelectedIndex());
        });

        previousButton.setOnAction(actionEvent -> {
            contentTable.getSelectionModel().select(contentTable.getSelectionModel().getSelectedIndex() - 1);
            playSelectedSong(contentTable.getSelectionModel().getSelectedIndex());
        });
    }

    private void configurePlayButton() {
        ToggleButton playButton = controlPaneController.getPlayButton();
        playButton.selectedProperty().addListener(observable -> {
            if (playButton.isSelected()) {
                playButton.setStyle(playButtonStyle(stopImage));
                mp3Player.play();
                playButton.setSelected(true);
            } else {
                playButton.setStyle(playButtonStyle(playImage));
                mp3Player.stop();
            }
        });
    }

    private String playButtonStyle(String image) {
        return ("-fx-background-image: url('" + image + "');" +
                "-fx-background-repeat: no-repeat;" +
                "-fx-background-position: center;" +
                "-fx-background-size: 80% 80%;n" +
                "-fx-background-color: transparent;");
    }

    private void openFileMenu() {
        MenuItem fileMenuItem = menuPaneController.getFileMenuItem();
        fileMenuItem.setOnAction(actionEvent -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Mp3", "*.mp3"));
            File file = fileChooser.showOpenDialog(new Stage());
            try {
                contentPaneController.getContentTable().getItems().add(SongFileParser.createSong(file));
                printMessage("File loaded: " + file.getName());
            } catch (Exception e) {
                printMessage("Cannot open mp3 file: " + file.getName());
            }
        });
    }

    private void openDiretoryMenu() {
        MenuItem dirMenuItem = menuPaneController.getDirMenuItem();
        dirMenuItem.setOnAction(actionEvent -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File directory = directoryChooser.showDialog(new Stage());
            try {
                contentPaneController.getContentTable().getItems().addAll(SongFileParser.createSongsList(directory));
                printMessage("Directory loaded: " + directory.getName());
            } catch (Exception e) {
                printMessage("Cannot open directory: " + directory.getName());
            }
        });
    }

    private void printMessage(String message){
        controlPaneController.getMessageTextField().setText(message);
    }
}